---
title: Why C?
subtitle: Why do we still use C?
date: 2020-03-17
tags: ["gcc", "c", "compiler", "blog post", "no code"]
author: Abraham Raji
---

C programming language has around for a while, it was created in Bell Laboratories in 1972 to build the Unix operating system. It's undoubtedly the most popular systems programming language in the world today. There are few reasons for this:
- First, it’s a very mature language that had been around for a long time. So, there is a large community around it. It's taught in every computer science or engineering degree course without exceptions for years now.
- C has a very lightweight [runtime](https://stackoverflow.com/questions/3900549/what-is-runtime "run-time"), so it doesn't require powerful hardware function properly. This in turn help hardware manufacturers and product developers to work with low powered and cost effective hardware.
- We have access to low-level programming through C. C supports inline assemblers and let's us to optimize the hardware directly.
- C is highly portable. C (namely the GCC compiler) is available in arguably every hardware architecture out. So you could run code written in C on a wide variety of hardware combinations. Most modern high-level languages implement multiple levels of abstraction that not even developers who have been working with these languages have an idea how a CPU actually executes their code.  By contrast, C's combination of high-level abstraction and low-level control keeps developers mindful of precisely how a CPU functions - leading to better, faster, cleaner code.
- Since C has been around for a while and has been the default systems programming language for years now, every programming language out there has some way to interface with C, so it allows you to do things like divide your program into modules and write a certain module in a language that would be better for the function of the program.
- No company holds monopoly over the development of C, so the community is always in control.

C also puts a lot of responsibility in the hands of the programmer. A lot of important functions such as garbage collection and memory management is in the hands of the programmer. Some modern languages implement garbage collectors which are nothing but pieces of software that run during run-time of a program allocating and reclaiming memory as per it's scope, some garbage collectors also provide memory safety. This is to try and take care of these tasks through the programming language itself take the responsibility off the programmer. But more often than not these garbage collectors add more burden to the hardware in terms of processing power and memory and often exhibit random behavior.

## Is C loosing it's popularity?
If we objectively look at this situation, almost all system software, programming languages, assembles, interpreters, databases are all still written partly or completely in C but unlike 1972 most software today are not system software.  There is just more software out there right now and there is a great demand for web based software. The entry level barriers to programming to is almost non-existent and tutorials available are available online for anyone who wants to learn. Modern day developers rely on programming languages that provide convenient abstractions and language design that has been tailored to their application. Right now with the boom in web technologies programming languages like javascript, ruby and python are on the rise. C has not lost it's place.

There have been attempts to replace C before and continue even today. The best attempt till date is the [the Rust language](https://rust-lang.org/ "rust"). It provides a lot of features that C lacks and is definitely worth your attention but it has yet to see wide spread adoption as a replacement for C. A software gains a certain amount of maturity with time, but when one simply re-writes the entire program in a different language it goes back to being 'beta software' and there are considerably less rust developers. These are the two things that are preventing wide spread adoption of Rust.

There's a lot of buzz around the language [Go](https://golang.org/ "go") too, but fundamentally Go doesn't try to replace the way Rust tries to. But what these two languages will grow out into is a matter of debate. All things considered this is an exciting time to be alive.

## Why should you still learn C?
- The first and foremost reason is that C allows you to get a better idea of what's happening under the hood. Since most programming languages take care of a lot of things for the developer and don't really think about it. However experienced developers are aware of what happens under the hood and often optimize their code accordingly. Newcomers on the other hand could benefit from learning the language like C as it would give them a bit of insight what the machine might actually be doing.

- C is often described as the Lingua Franca for communicating between developers and is popular among most computer scientists so lot of new ideas are first implemented in C. So if you know C you'll be able to port those algorithms, procedures, methods, etc. to your preferred language.

So yeah there you go. 
